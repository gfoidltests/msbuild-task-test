﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Translator
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length != 2) return;

			string dictionary = args[0];
			string[] files = args[1].Split(';');

			string msg = string.Format(
				"Dictionary: {0}\n" +
				"Files:      {1}\n" +
				"Time:       {2}\n" +
				"-----------------------------------\n",
				dictionary,
				string.Join(";", files),
				DateTime.Now);

			File.AppendAllText(
				Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "log.txt"),
				msg);

			Dictionary<string, string> dic = GetDictionary(dictionary);
			foreach (string file in files)
				Translate(dic, file);
		}
		//---------------------------------------------------------------------
		private static void Translate(Dictionary<string, string> dic, string file)
		{
			string content = File.ReadAllText(file);

			content = Regex.Replace(content, @"\$\{(?<token>[^\}]*)\}", match =>
			{
				string replacement;
				if (dic.TryGetValue(match.Groups["token"].Value, out replacement))
					return replacement;

				return match.Value;
			});

			File.WriteAllText(file, content);
		}
		//---------------------------------------------------------------------
		private static Dictionary<string, string> GetDictionary(string fileName)
		{
			Dictionary<string, string> dic = new Dictionary<string, string>();

			using (StreamReader sr = File.OpenText(fileName))
			{
				while (!sr.EndOfStream)
				{
					string[] cols = sr.ReadLine().Split(';');

					dic[cols[0]] = cols[1];
				}
			}

			return dic;
		}
	}
}