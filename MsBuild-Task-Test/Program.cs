﻿using System;
using System.IO;

namespace MsBuild_Task_Test
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				string content = null;

				content = File.ReadAllText("de/text.md");
				Console.WriteLine(content);

				content = File.ReadAllText("en/text.md");
				Console.WriteLine(content);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			Console.WriteLine("\nEnd.");
			Console.ReadKey();
		}
	}
}