﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace TestTask
{
	public class MyTask : Task
	{
		[Required]
		public string Name { get; set; }

		public int Age { get; set; }

		[Output]
		public string Message { get; private set; }
		//---------------------------------------------------------------------
		public override bool Execute()
		{
			this.Log.LogMessage(MessageImportance.High, "----- Hallo {0} -----", this.Name);
			this.Log.LogMessage("Alter: {0}", this.Age);

			this.Message = string.Format("Hallo {0} mit Alter {1}", this.Name, this.Age);

			return this.Name != "Adolf" && !Log.HasLoggedErrors;
		}
	}
}