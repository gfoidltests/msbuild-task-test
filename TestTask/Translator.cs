﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace TestTask
{
	public class Translator : Task
	{
		private const string Pattern = @"\$\{(?<token>[^\}]*)\}";
		//---------------------------------------------------------------------
		[Required]
		public string Dictionary { get; set; }

		[Required]
		public ITaskItem[] Files { get; set; }
		//---------------------------------------------------------------------
		public override bool Execute()
		{
			if (!File.Exists(this.Dictionary))
			{
				this.Log.LogError("Wörterbuch für '{0}' nicht vorhanden. Erwartet an {1}", Path.GetFileName(this.Dictionary), this.Dictionary);
				return false;
			}

			if (this.Files.Length == 0) return !this.Log.HasLoggedErrors;
#if DEBUG
			this.Log.LogMessage(MessageImportance.High, "--------------------- #### ---------------------");
			this.Log.LogMessage(MessageImportance.High, this.Dictionary);
			this.Log.LogMessage(MessageImportance.High, string.Join(";", this.GetFileNames()));
			this.Log.LogMessage(MessageImportance.High, "--------------------- #### ---------------------");

			string msg = string.Format(
				"Dictionary: {0}\n" +
				"Files:      {1}\n" +
				"Time:       {2}\n" +
				"-----------------------------------\n",
				this.Dictionary,
				string.Join(";", this.GetFileNames()),
				DateTime.Now);

			File.AppendAllText(
				Path.Combine(Path.GetDirectoryName(typeof(Translator).Assembly.Location), "logTranslator.txt"),
				msg);
#endif
			Dictionary<string, string> dic = GetDictionary(this.Dictionary);
			foreach (string file in this.GetFileNames())
				Translate(dic, file);

			return !Log.HasLoggedErrors;
		}
		//---------------------------------------------------------------------
		private IEnumerable<string> GetFileNames()
		{
			foreach (var file in this.Files)
				yield return file.ItemSpec;
		}
		//---------------------------------------------------------------------
		private static Dictionary<string, string> GetDictionary(string fileName)
		{
			Dictionary<string, string> dic = new Dictionary<string, string>();

			using (StreamReader sr = File.OpenText(fileName))
			{
				while (!sr.EndOfStream)
				{
					string[] cols = sr.ReadLine().Split(';');

					dic[cols[0]] = cols[1];
				}
			}

			return dic;
		}
		//---------------------------------------------------------------------
		private static void Translate(Dictionary<string, string> dic, string file)
		{
			string content = File.ReadAllText(file);

			content = Regex.Replace(content, Pattern, match =>
			{
				string replacement;
				if (dic.TryGetValue(match.Groups["token"].Value, out replacement))
					return replacement;

				return match.Value;
			});

			File.WriteAllText(file, content);
		}
	}
}