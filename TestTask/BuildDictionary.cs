﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace TestTask
{
	public class BuildDictionary : Task
	{
		private const string Pattern = @"\$\{(?<token>[^\}]*)\}";
		//---------------------------------------------------------------------
		[Required]
		public string Dictionary { get; set; }

		[Required]
		public ITaskItem[] Templates { get; set; }
		//---------------------------------------------------------------------
		public override bool Execute()
		{
#if DEBUG
			this.Log.LogMessage("# Templates = {0}", this.Templates.Length);
#endif
			if (this.Templates.Length == 0) return !this.Log.HasLoggedErrors;

			IEnumerable<string> tokens = this.GetTokens();
			HashSet<string> tokenSet = new HashSet<string>(tokens);
			this.WriteDictionary(tokenSet);

			return !Log.HasLoggedErrors;
		}
		//---------------------------------------------------------------------
		private IEnumerable<string> GetTokens()
		{
			foreach (var taskItem in this.Templates)
			{
				string text = File.ReadAllText(taskItem.ItemSpec);

				MatchCollection matches = Regex.Matches(text, Pattern);

				foreach (Match match in matches)
				{
					yield return match.Groups["token"].Value;
				}
			}
		}
		//---------------------------------------------------------------------
		private void WriteDictionary(IEnumerable<string> tokens)
		{
			using (StreamWriter sw = File.CreateText(this.Dictionary))
			{
				foreach (string token in tokens)
					sw.WriteLine("{0};", token);
			}
		}
	}
}